import ApexCharts from "apexcharts";
import Swal from "sweetalert2";
import moment from "moment/moment";
import 'moment/locale/th';

moment.locale('th');

export function genChart1(element){
    
    let options = {
        chart:{
            type:'bar',
            toolbar:{
                show:true
            },
            stacked:true
        },
        title:{
            text:'Sale Report Last 10 Years [Bar]',
            align: 'center', // center ,left ,right
            style:{
                fontSize: '20px',
                fontFamily:'Itim',
                color:'#1b2c3d'
            }
        },
        subtitle:{
            text:'วันที่เรัยกดู ' + moment().format('LLL'),
            align: 'center',
            style:{
                fontSize: '12px',
                fontFamily:'Itim',
                color:'#C30502'
            }
        },
        // dataLabels:{
        //     enabled: false
        // },
        plotOptions:{
            bar:{
                horizontal:false,
                dataLabels:{
                    position: 'top',
                }
            }
        },
        stroke:{
            curve:'smooth',
            width: 2
        },
        theme:{
            palette:'palette5' // palette theme 1 to 10
        },
        series: [
            {
                name: 'sales item 1',
                data: [30,40,50,90,125,90,50,40,30]
            },
            {
                name: 'sales item 2',
                data: [125,90,50,40,30,40,50,90,125]
            }
        ],
        legend:{
            show:true,
            position: 'bottom',
            horizontalAlign:'right'
        },
        xaxis: {
            title:{
                text:'years',
                style:{
                    fontFamily:'Itim'
                }
            },
            labels:{
                rotate: -45,
                rotateAlways: true,
                style:{
                    fontSize: 12,
                    fontFamily:'Itim'
                }
            },
            categories: [1991,1992,1993,1994,1995,1996,1997, 1998,1999]
        },
        yaxis:{
            title:{
                text:'sales',
                style:{
                    color:'#C30502',
                    fontFamily:'Itim'
                }
            }
        }
    }

    let chart = new ApexCharts(element, options);

    chart.render();
}