import ApexCharts from "apexcharts";
import axios from "axios";
import Swal from "sweetalert2";
import moment from "moment/moment";
import 'moment/locale/th';

moment.locale('th');

export async function genChart3(element){
    
    let options = {
        chart:{
            type:'line',
            toolbar:{
                show:true
            }
        },
        title:{
            text:'Sale Report Last 10 Years [Line]',
            align: 'center', // center ,left ,right
            style:{
                fontSize: '20px',
                fontFamily:'Itim',
                color:'#1b2c3d'
            }
        },
        subtitle:{
            text:'วันที่เรัยกดู ' + moment().format('LLL') + ' [Backend]',
            align: 'center',
            style:{
                fontSize: '12px',
                fontFamily:'Itim',
                color:'#C30502'
            }
        },
        // dataLabels:{
        //     enabled: false
        // },
        plotOptions:{
            bar:{
                horizontal:false,
                dataLabels:{
                    position: 'top',
                    
                }
            }
        },
        stroke:{
            curve:'smooth', // smooth / straight /stepline
            width: 2
        },
        theme:{
            palette:'palette5' // palette theme 1 to 10
        },
        series: [
          
        ],
        noData:{
            text:'Loading...',
            fontFamily:'Itim'
        },
        xaxis: {
            // type:'category',
            tickPlacement:'on',
            title:{
                text:'years',
                style:{
                    fontFamily:'Itim'
                }
            },
            labels:{
                rotate: -45,
                rotateAlways: true,
                style:{
                    fontSize: 12,
                    fontFamily:'Itim'
                }
            },
            categories: [1991,1992,1993,1994,1995,1996,1997, 1998,1999]
        },
        yaxis:{
            title:{
                text:'sales',
                style:{
                    color:'#C30502',
                    fontFamily:'Itim'
                }
            }
        }
    }

    let chart = new ApexCharts(element, options);

    //get data from backend
    
    
    chart.render();

    const url = 'http://my-json-server.typicode.com/apexcharts/apexcharts.js/yearly';
    const jsonData = await axios.get(url);

    const revertData = [...jsonData.data].reverse();

    chart.updateSeries([
        {
            name: 'sales item 1',
            data: jsonData.data
        },
        {
            name: 'sales item 2',
            data: revertData
        }
    ]);
}