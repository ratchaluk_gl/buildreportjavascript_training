import ApexCharts from "apexcharts";
import axios from "axios";
import Swal from "sweetalert2";
import moment from "moment/moment";
import 'moment/locale/th';

moment.locale('th');

export function genChart4(element){
    
    let options = {
        chart:{
            type:'donut',
            toolbar:{
                show:true
            }
        },
        title:{
            text:'Sale Report Last 10 Years [Pie & Donut]',
            align: 'center', // center ,left ,right
            style:{
                fontSize: '20px',
                fontFamily:'Itim',
                color:'#1b2c3d'
            },
            offsetX: -50, //margin
        },
        subtitle:{
            text:'วันที่เรัยกดู ' + moment().format('LLL'),
            align: 'center',
            style:{
                fontSize: '12px',
                fontFamily:'Itim',
                color:'#C30502'
            },
            offsetX: -50, 
        },
        plotOptions: {
            pie: {
                customScale: 0.8,
                donut: {
                    size: '35%'
                }
            }
        },
        stroke:{
            curve:'smooth', // smooth / straight /stepline
            width: 2
        },
        theme:{
            palette:'palette5' // palette theme 1 to 10
        },
        series: [44, 55, 13, 33],
        labels: ['Apple', 'Mango', 'Orange', 'Watermelon'],
        noData:{
            text:'Loading...',
            fontFamily:'Itim'
        },
        
    }

    let chart = new ApexCharts(element, options);


    chart.render();

    
}