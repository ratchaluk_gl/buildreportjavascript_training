import './style.css'
import javascriptLogo from './javascript.svg'
import viteLogo from '/vite.svg'
import { genChart1 } from './chart1'
import { genChart2 } from './chart2'
import { genChart3 } from './chart3'
import { genChart4 } from './chart4'


document.querySelector('#app').innerHTML = `
  <div>
    <a href="javascript:void(0);" class="w-50">
      <img src="${javascriptLogo}" class="logo vanilla w-75" alt="JavaScript logo" />
    </a>
    <div class="w-100">
      <div id="chart1" class="border rounded-3 bg-light chart w-100"></div>
    </div>
  </div>
  <div>
    <div class="w-100">
      <div id="chart2" class="border rounded-3 bg-light chart w-100"></div>
    </div>
  </div>
  <div>
    <div class="w-100">
      <div id="chart3" class="border rounded-3 bg-light chart w-100"></div>
    </div>
  </div>
  <div>
    <div class="w-100">
      <div id="chart4" class="border rounded-3 bg-light chart w-100"></div>
    </div>
  </div>
`
window.addEventListener('load',(ev)=>{
  genChart1(document.querySelector("#chart1"));
  genChart2(document.querySelector("#chart2"));
  genChart3(document.querySelector("#chart3"));
  genChart4(document.querySelector("#chart4"));
});
