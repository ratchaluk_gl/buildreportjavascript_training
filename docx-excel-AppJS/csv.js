import * as XLSX from "xlsx";
import axios from "axios";
import moment from "moment/moment";
import 'moment/locale/th';
import { saveAs } from "file-saver";

moment.locale('th');

export function genCSV(element){
    element.addEventListener('click',async ()=>{
        
        //axios
        let hospitals =[];
        const responseDataTable = await axios.get('https://api.codingthailand.com/api/hospital2?page=1&page_size=100')
        hospitals = responseDataTable.data.data;

        // create worksheet
        let worksheet = XLSX.utils.json_to_sheet(hospitals);

        //export to csv
        let csv = XLSX.utils.sheet_to_csv(worksheet,{ FS: ',' });

        //add BOM to read thai text on excel
        let bom = "\uFEFF";
        let csvWithBOM = bom + csv;

        //content header type csv
        let blob = new Blob([/*csv*/csvWithBOM],{ type: 'text/csv;charset=utf-8' });

        //export
        saveAs(blob,'hospital.csv');
        
    });

}