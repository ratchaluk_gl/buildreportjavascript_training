import { AlignmentType, convertMillimetersToTwip, Document, HeadingLevel, Packer, PageOrientation, Paragraph, TextRun } from "docx";
import axios from "axios";
import Swal from "sweetalert2";
import moment from "moment/moment";
import 'moment/locale/th';
import { saveAs } from "file-saver";

moment.locale('th');

export function genDocx1(element){
    element.addEventListener('click',()=>{
        //alert('Hello Docx today => ' + moment().format('LLL'))
        const doc = new Document({
            creator: "John Wick",
            title: 'Daily Reports',
            customProperties:[
                { name: 'Update By', value: 'John Wich'}
            ],
            background:{
                color:'#ffffff' //only hex code
            },
            styles:{
                characterStyles:[
                    {
                        id:'customParagraph-1',
                        name:'for paragraph',
                        basedOn: 'Normal',
                        run:{
                            font: 'Prompt',
                            size: '12pt',
                            italics: true,
                            alignment: AlignmentType.THAI_DISTRIBUTE,
                            color:'3C3D3C',
                        }
                    }
                ]
            },
            sections: [
                {
                    properties:{ //page properties
                        page:{
                            size:{
                                orientation: PageOrientation.PORTRAIT,
                                height:`300mm`,
                                width: `210mm`
                            },
                            margin:{
                                top: convertMillimetersToTwip(10),
                                right: `20mm`,
                                bottom:`10mm`,
                                left:`20mm`,
                                // header:'', //margin of header
                                // footer:''
                            }
                        }
                    },
                    children: [
                        new Paragraph(
                            { 
                                text: "Hello John Wick" ,
                                heading:HeadingLevel.TITLE,
                                alignment: AlignmentType.CENTER,
                                
                            },
                        ),
                        new Paragraph(
                            {
                                children:[
                                    new TextRun(
                                        { 
                                            text: 'This is 1st Paragraph , ภาษาไทย นั้น นั่น ปฏิบัติ สุทธิ สิทธิ์ การันต์',
                                            bold:false,
                                            font:'Prompt',
                                            size: '12pt',
                                            color:'#05270D'
                                        }
                                    ),
                                    new TextRun(
                                        { 
                                            text: ' /This is 2nd Paragraph ,lorem 100 js lorem text test to text run paragraph 100 lorem , ภาษาไทย นั้น นั่น ปฏิบัติ สุทธิ สิทธิ์ การันต์',
                                            style:'customParagraph-1',
                                        }
                                    )
                                ]
                            }
                        ),
                        new Paragraph(
                            {
                                alignment:AlignmentType.THAI_DISTRIBUTE,
                                spacing:{
                                  before:15,
                                  after: 15  
                                },
                                children:[
                                    new TextRun(
                                        { 
                                            text: '\tภาษาไทย นั้น นั่น ปฏิบัติ สุทธิ สิทธิ์ การันต์ ภาษาไทย นั้น นั่น ปฏิบัติ สุทธิ สิทธิ์ การันต์ ภาษาไทย นั้น นั่น ปฏิบัติ สุทธิ สิทธิ์ การันต์ ภาษาไทย นั้น นั่น ปฏิบัติ สุทธิ สิทธิ์ การันต์ ภาษาไทย นั้น นั่น ปฏิบัติ สุทธิ สิทธิ์ การันต์ ภาษาไทย นั้น นั่น ปฏิบัติ สุทธิ สิทธิ์ การันต์ ภาษาไทย นั้น นั่น ปฏิบัติ สุทธิ สิทธิ์ การันต์ ภาษาไทย นั้น นั่น ปฏิบัติ สุทธิ สิทธิ์ การันต์ ภาษาไทย นั้น นั่น ปฏิบัติ สุทธิ สิทธิ์ การันต์ ภาษาไทย นั้น นั่น ปฏิบัติ สุทธิ สิทธิ์ การันต์ ภาษาไทย นั้น นั่น ปฏิบัติ สุทธิ สิทธิ์ การันต์ ภาษาไทย นั้น นั่น ปฏิบัติ สุทธิ สิทธิ์ การันต์',
                                            bold:false,
                                            font:'Prompt',
                                            size: '12pt',
                                            color:'#05270D'
                                        }
                                    ),
                                    
                                ]
                            }
                        ),
                    ]
                }
            ]
        }); // end doc
        Packer.toBlob(doc).then((blob)=>{
            saveAs(blob , "report1.docx");
        });
    });

}