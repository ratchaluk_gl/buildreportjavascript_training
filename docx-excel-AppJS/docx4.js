import { AlignmentType, convertMillimetersToTwip, Document, Footer, Header, HeadingLevel, ImageRun, Packer, PageNumber, PageOrientation, Paragraph, Table, TableCell, TableRow, TextRun, VerticalAlign, WidthType } from "docx";
import { Buffer } from "buffer";
import axios from "axios";
import Swal from "sweetalert2";
import moment from "moment/moment";
import 'moment/locale/th';
import { saveAs } from "file-saver";

moment.locale('th');

export function genDocx4(element){
    element.addEventListener('click',async ()=>{
        //get image from url via axios
        
        const responseLogo = await axios.get('https://codingthailand.com/site/img/logo_cct.png',{
            responseType:'arraybuffer'
        })

        async function getImage(url){
            const responseImg = await axios.get(url,{
                responseType:'arraybuffer'
            })
            return responseImg.data
        }
        let hospitals =[];
        //const responseDataTable = await axios.get('https://api.codingthailand.com/api/hospital2?page=1&page_size=100')
        const responseDataTable = await axios.get('https://api.codingthailand.com/api/course')
        
        hospitals = responseDataTable.data.data;
        
        let tableCell =[];
        tableCell = hospitals.map(({ id , picture, detail})=>{
            return new TableRow({
                children:[
                    new TableCell({
                        verticalAlign:VerticalAlign.CENTER,
                        children:[
                            new Paragraph({
                                alignment:AlignmentType.CENTER,
                                text: id.toString(),
                                style:'tableCellStyle'
                            })
                        ]
                    }),
                    new TableCell({
                        verticalAlign:VerticalAlign.CENTER,
                        children:[
                            new Paragraph({
                                alignment: AlignmentType.CENTER,
                                children:[
                                    new ImageRun({
                                        data: getImage(picture), //bufferArray 
                                        transformation:{
                                            width:90,
                                            height:50
                                        }
                                    })
                                ]
                            })
                        ]
                    }),
                    new TableCell({
                        margins:{
                            left:convertMillimetersToTwip(3)
                        },
                        children:[
                            new Paragraph({
                                text: detail,
                                style:'tableCellStyle'
                            })
                        ]
                    }),
                ]
            })
        })

        const doc = new Document({
            creator: "John Wick",
            title: 'Daily Reports',
            customProperties:[
                { name: 'Update By', value: 'John Wich'}
            ],
            background:{
                color:'#ffffff' //only hex code
            },
            styles:{
                characterStyles:[
                    {
                        id:'customParagraph-1',
                        name:'for paragraph',
                        basedOn: 'Normal',
                        run:{
                            font: 'Prompt',
                            size: '8pt',
                            italics: true,
                            alignment: AlignmentType.THAI_DISTRIBUTE,
                            color:'3C3D3C',
                        }
                    }
                ],
                paragraphStyles:[
                    {
                        id:'tableHeaderStyle',
                        name:'for table header',
                        basedOn: 'Normal',
                        run:{
                            font: 'Prompt',
                            size: '10pt',
                            bold: true,
                            color:'3C3D3C',
                        },
                        paragraph:{
                            alignment:AlignmentType.CENTER
                        }
                    },
                    {
                        id:'tableCellStyle',
                        name:'for table header',
                        basedOn: 'Normal',
                        run:{
                            font: 'Prompt',
                            size: '9pt',
                            color:'3C3D3C',
                        },
                        // paragraph:{
                        //     alignment:AlignmentType.CENTER
                        // }
                    }
                ]
            },
            sections: [
                {
                    headers:{
                        default: new Header({
                            children:[
                                new Paragraph({
                                    alignment: AlignmentType.RIGHT,
                                    children:[
                                        new TextRun({
                                             size:'8pt',
                                            // text:'หน้า '+ PageNumber.CURRENT + '/' + PageNumber.TOTAL_PAGES,
                                            children:[
                                                "หน้า ", PageNumber.CURRENT , '/',PageNumber.TOTAL_PAGES
                                            ]
                                        })
                                        // "Page #:", PageNumber.CURRENT
                                    ]
                                })
                                
                            ]
                        })
                    }, // end header
                    footers:{
                        default: new Footer({
                            children:[
                                new Paragraph({
                                    alignment:AlignmentType.CENTER,
                                    text: 'วันที่ '+ moment().format('LLL'),
                                })
                            ]
                        })
                    }, //end footer
                    properties:{ //page properties
                        page:{
                            size:{
                                orientation: PageOrientation.PORTRAIT,
                                height:`300mm`,
                                width: `210mm`
                            },
                            margin:{
                                top: convertMillimetersToTwip(10),
                                right: `20mm`,
                                bottom:`10mm`,
                                left:`20mm`,
                                // header:'', //margin of header
                                // footer:''
                            },
                        }
                    },
                    children: [
                        new Paragraph({ //images
                            alignment:AlignmentType.CENTER,
                            children:[
                                new ImageRun({
                                    data: responseLogo.data, //bufferArray 
                                    //data: Buffer.from(responseLogo.data,'binary').toString('base64'),
                                    transformation:{
                                        width:150,
                                        height:130
                                    },
                                })
                            ]
                        }),
                        new Paragraph(
                            { 
                                text: "Coding Thailand Course" ,
                                heading:HeadingLevel.TITLE,
                                alignment: AlignmentType.CENTER,
                                spacing:{
                                    after: 250
                                }
                            },
                        ),
                        new Table({
                            
                            width:{ size: 90 , type:WidthType.PERCENTAGE },
                            alignment:AlignmentType.CENTER,
                            rows:[
                                new TableRow({
                                    tableHeader:true,
                                    verticalAlign:VerticalAlign.CENTER,
                                    children:[
                                        new TableCell({
                                            width:{ size: '10%' },
                                            children:[
                                                new Paragraph({
                                                    //alignment:AlignmentType.CENTER,
                                                    text:'Id',
                                                    style:'tableHeaderStyle'
                                                })
                                            ]
                                        }),
                                        new TableCell({
                                            width:{ size: '15%' },
                                            children:[
                                                new Paragraph({
                                                    //alignment:AlignmentType.CENTER,
                                                    text:'Picture',
                                                    style:'tableHeaderStyle'
                                                })
                                            ]
                                        }),
                                        new TableCell({
                                            
                                            children:[
                                                new Paragraph({
                                                    //alignment:AlignmentType.CENTER,
                                                    
                                                    text:'Detail',
                                                    style:'tableHeaderStyle'
                                                })
                                            ]
                                        })
                                    ]
                                }), //header row
                                
                                ...tableCell
                            ]
                        }),
                        // new Paragraph(
                        //     { 
                        //         pageBreakBefore:true, //new page - start before this
                        //         text: "Hotel List in Thailand" ,
                        //         heading:HeadingLevel.TITLE,
                        //         alignment: AlignmentType.CENTER,
                                
                        //     },
                        // ),
                        // new Paragraph(
                        //     { 
                        //         pageBreakBefore:true, //new page - start before this
                        //         text: "Resort List in Thailand" ,
                        //         heading:HeadingLevel.TITLE,
                        //         alignment: AlignmentType.CENTER,
                        //     },
                        // ),
                        
                    ]
                }
            ]
        }); // end doc
        Packer.toBlob(doc).then((blob)=>{
            saveAs(blob , "report2.docx");
        });
    });

}