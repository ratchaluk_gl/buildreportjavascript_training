import * as XLSX from "xlsx";
import axios from "axios";
import moment from "moment/moment";
import 'moment/locale/th';

moment.locale('th');

export function genExcel1(element){
    element.addEventListener('click',async ()=>{
        //Example
        let users =[
            { id:1 , fullName : 'John Wick' },
            { id:2 , fullName : 'John Wack' },
            { id:3 , fullName : 'John Wock' },
        ];
        //axios
        let hospitals =[];
        const responseDataTable = await axios.get('https://api.codingthailand.com/api/hospital2?page=1&page_size=100')
        hospitals = responseDataTable.data.data;

        // create worksheet
        let worksheet = XLSX.utils.json_to_sheet(hospitals);

        //custom column width
        const maxColWidth = hospitals.reduce((w,r)=> Math.max(w,r.h_name.length), 50); // calculate the most width of data in column
        worksheet['!cols'] = [
            { wch: 10 },
            { wch: 20 },
            { wch: maxColWidth },
        ];

        //custom header
        XLSX.utils.sheet_add_aoa(worksheet,[
            ['Id','Code','Hospital Name']
        ],{
            origin: 'A1'
        });

        // create workbook
        let workbook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(workbook,worksheet,'EXCEL 1 Example');

        // write file and export
        XLSX.writeFile(workbook,'User.xlsx');

    });

}