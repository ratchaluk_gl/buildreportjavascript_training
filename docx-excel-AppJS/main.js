import './style.css'
import javascriptLogo from './javascript.svg'
import viteLogo from '/vite.svg'
import { setupCounter } from './counter.js'
import { genDocx1 } from './docx1'
import { genDocx2 } from './docx2'
import { genDocx3 } from './docx3'
import { genDocx4 } from './docx4'
import { genExcel1 } from './excel1'
import { genCSV } from './csv'

document.querySelector('#app').innerHTML = `
  <div>
    
    <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank" class="w-100">
      <img src="${javascriptLogo}" class="logo vanilla" alt="JavaScript logo" />
    </a>
    <h1 style="color:white;">JavaScript Docx</h1>
    <div class="card">
      <div class="row">
        <div class="col-6">
          <span> DOCX </span><br />
          <button id="btnDocx1" class="mt-1 w-100" type="button">DOCX 1 Example</button>
        </div>
        <div class="col-6">
          <span> EXCEL </span><br />
          <button id="btnExcel1" class="mt-1 w-100" type="button">EXCEL 1 Example</button>
        </div>
      </div>  
      <div class="row">
        <div class="col-6">
          <button id="btnDocx2" class="mt-1 w-100" type="button">DOCX 2 Header , Footer ,Image ,Table</button>
        </div>
        <div class="col-6">
          <button id="btnExcel2" class="mt-1 w-100" type="button">EXCEL 2 Export to CSV</button>
        </div>
      </div>  
      <div class="row">
        <div class="col-6">
          <button id="btnDocx3" class="mt-1 w-100" type="button">DOCX 3 Get Data From API</button>
        </div>
        <div class="col-6">
          
        </div>
      </div>  
      <div class="row">
        <div class="col-6">
          <button id="btnDocx4" class="mt-1 w-100" type="button">DOCX 4 Workshop</button>
        </div>
        <div class="col-6">
          
        </div>
      </div>  
    </div>
    <p class="read-the-docs">
      Click on the List of Button for Example
    </p>
    
  </div>
`

//setupCounter(document.querySelector('#counter'))
genDocx1(document.querySelector('#btnDocx1'))
genDocx2(document.querySelector('#btnDocx2'))
genDocx3(document.querySelector('#btnDocx3'))
genDocx4(document.querySelector('#btnDocx4'))
genExcel1(document.querySelector('#btnExcel1'))
genCSV(document.querySelector('#btnExcel2'))


