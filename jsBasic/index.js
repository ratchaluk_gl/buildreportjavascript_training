
//import mySum2 from "./mySum"; // run for js
import { mySum3,mySum2} from "./mySum"; //multiple import
const user = { fullName: 'John Wick',job: 'assasin'};
const { fullName , job } = user;
//console.log(fullName , job);

const department = {addr : 'The Continental'};
const newUser = { ...user, ...department}; //merge object
//console.log(newUser);

//Rest Param
const mySum =(...args)=> console.log(args.length);
const mySum2 =(...args)=> args.reduce((prev,curr)=> prev + curr,0);
mySum(1,2,5,2,5,5,4,6);


console.log(mySum2(20,58,345,5,544));

