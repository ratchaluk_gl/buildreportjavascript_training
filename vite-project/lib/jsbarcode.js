import JsBarcode from "jsbarcode";

export function textToBase64Barcode(text,fromat){
    var canvas = document.createElement("canvas");
    JsBarcode(canvas, text, {
      format: fromat,
      height: 40,
      //width:5,
      //displayValue:false,
      margin: 5
    });
    return canvas.toDataURL("image/png");
  }