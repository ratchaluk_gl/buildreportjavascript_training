import * as pdfMake from "pdfmake/build/pdfmake";
import * as pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;

//thai font
pdfMake.fonts ={
    thSarabunNew:{
        normal:'https://codingthailand.com/site/fonts/th/THSarabunNew.ttf',
        bold: 'https://codingthailand.com/site/fonts/th/THSarabunNewBold.ttf',
        italics: 'https://codingthailand.com/site/fonts/th/THSarabunNewItalic.ttf',
        bolditalics: 'https://codingthailand.com/site/fonts/th/THSarabunNewBoldItalic.ttf'
    }
};

export { pdfMake };