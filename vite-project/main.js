import './style.css'
import javascriptLogo from './javascript.svg'
import viteLogo from '/vite.svg'
import { setupCounter } from './counter.js'
import { genPDF1 } from './pdf1'
import { genPDF2 } from './pdf2'
import { genPDF3 } from './pdf3'
import { genPDF4 } from './pdf4'
import { genPDF5 } from './pdf5'
import { genPDF6 } from './pdf6'
import { genPDF7 } from './pdf7'

document.querySelector('#app').innerHTML = `
  <div>
    
    <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank" class="w-100">
      <img src="${javascriptLogo}" class="logo vanilla" alt="JavaScript logo" />
    </a>
    <h1 style="color:white;">Hello JavaScript</h1>
    <div class="card">
      <button id="counter" type="button"></button>
      <button id="btnPDF1" class="mt-1" type="button">PDF 1 Example</button>
      <button id="btnPDF2" class="mt-1" type="button">PDF 2 Example Page</button>
      <button id="btnPDF3" class="mt-1" type="button">PDF 3 Example Text/Header/Footer/Image</button>
      <button id="btnPDF4" class="mt-1" type="button">PDF 4 Example Table</button>
      <button id="btnPDF5" class="mt-1" type="button">PDF 5 Example Get Data From Backend</button>
      <button id="btnPDF6" class="mt-1" type="button">PDF 6 Workshop</button>
      <button id="btnPDF7" class="mt-1" type="button">PDF 7 Workshop CERT.</button>
    </div>
    <p class="read-the-docs">
      Click on the List of Button for Example
    </p>
    
  </div>
`

setupCounter(document.querySelector('#counter'))
genPDF1(document.querySelector('#btnPDF1'))
genPDF2(document.querySelector('#btnPDF2'))
genPDF3(document.querySelector('#btnPDF3'))
genPDF4(document.querySelector('#btnPDF4'))
genPDF5(document.querySelector('#btnPDF5'))
genPDF6(document.querySelector('#btnPDF6'))
genPDF7(document.querySelector('#btnPDF7'))
