import { pdfMake } from "./lib/pdfmake";
export function genPDF1(element) {
    
    element.addEventListener('click', () => {
        //alert('Hello PDF 1');
        let docDefinition ={
          content:[
              {text:'Hello PDF Report',fontSize:30 ,alignment:'center',bold:true},
              {text:'"Custon Style"', style: 'header'},
              {text:'Hello pdfMake , สวัสดี รายงานวันนี้', style: 'paragraph'},
              {text:'สวัสดี รายงานวันนี้ ไม่มีอะไรให้ดู', style: 'paragraph'},
              {text:'สวัสดี รายงานวันนี้ ไม่รู้จะดูอะไร', style: 'paragraph'}
          ],
          styles:{
              header:{
                  color : 'red',
                  alignment: 'center',
                  italics:true
              },
              paragraph:{
                  color : 'black',
                  alignment: 'right',
                  italics:true
              }
          },
          defaultStyle:{
              font:'thSarabunNew',
              fontSize:16,
              color:'black'
          }
      };
      pdfMake.createPdf(docDefinition).open();
    })
  }
  