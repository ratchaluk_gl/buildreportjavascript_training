import { pdfMake } from "./lib/pdfmake";
import Swal, { SweetAlertArrayOptions,SweetAlertIcon } from "sweetalert2";
export function genPDF2(element) {
    
    element.addEventListener('click', () => {
        //alert('Hello PDF 2');
        // Swal.fire({
        //     title:'Hello PDF 2',
        //     icon: 'success'
        // });
        pdfMake.createPdf({
            pageSize:'A5',
            pageOrientation :'landscape',
            pageMargins:[20,10], //default 40
            watermark:{text :'Public Secret :D',opacity:0.2, bold:true ,color :'gray', angle:-45 /*detaul -45*/},
            info: {
                title: 'NT PDF',
                author: 'The Continental',
                subject: 'John',
                creator: 'John Wick',
                keywords: 'Somebody please! get this man a gun',
              },
            content: [
                {text :'Hello PDF 2',style:'header'},
                {text : 'text portrait paragraph',pageOrientation:'portrait',pageBreak:'before'}//pageBreak = new page
            ],
            styles:{
                header:{
                    font:'thSarabunNew',
                    bold:true,
                    color:'red',
                    fontSize: 22,
                    alignment:'center'
                }
            },
            defaultStyle : {
                font: 'thSarabunNew',
                fontSize : 16
            }
            
        }).open();
    });
  }
  