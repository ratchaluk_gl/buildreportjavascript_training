import { pdfMake } from "./lib/pdfmake";
import Swal, { SweetAlertArrayOptions,SweetAlertIcon } from "sweetalert2";
import moment from "moment/moment";
import 'moment/locale/th';
moment.locale('th');

export function genPDF3(element) {
    element.addEventListener('click', () => {
        //alert('Hello PDF 2');
        // Swal.fire({
        //     title:'Hello PDF 3',
        //     icon: 'success'
        // });
        pdfMake.createPdf({
            // userPassword:'123456',
            // ownerPassword:'987654',
            // permissions: {
            //     printing: 'lowResolution', //'highResolution'
            //     modifying: false,
            //     copying: false,
            //     annotating: true,
            //     fillingForms: true,
            //     contentAccessibility: true,
            //     documentAssembly: true
            //   },
            // header: [
            //     {text:'ซีเซียม 137',marginLeft:10}
            // ],
            // footer: {
            //     columns: [
            //     {text:'ที่มา : https://www.facebook.com/Hfocus.org',marginLeft:10},
            //     { text: new Date().toISOString(), alignment: 'right',marginRight:10 }
            //     ]
            // },
            header:(currentPage,pageCount,pageSize)=>{
                return {
                    columns:[
                        { text:'บทความน่าสนใจ',margin:10 },
                        { text:'หน้า '+ currentPage +'/'+ pageCount, alignment:'right',margin:10 }
                    ]
                }
            },
            footer:(currentPage,pageCount,pageSize)=>{
                return {
                    columns:[
                        { text:'ที่มา : https://www.facebook.com/Hfocus.org',margin:10 },
                        { text : moment().format('ll') ,margin:10 , alignment:'center'},
                        { text:'หน้า '+ currentPage +'/'+ pageCount, alignment:'right',margin:10 }
                    ]
                }
            },
            images: {
              logo: "https://fastly.picsum.photos/id/866/200/300.jpg?hmac=rcadCENKh4rD6MAp6V_ma-AyWv641M4iiOpe1RyFHeI",
              myImage:
                "https://codingthailand.com/site/img/codingthailand_logo.png",
            },
            content: [
              {
                image: "myImage",
                width: 60,
                alignment: "center",
                marginBottom: 20,
              },
              {
                image: "logo",
                width: 60,
                //alignment: "center",
                marginBottom: 20,
              },

              {text: 'ซีเซียม 137 คืออะไร?',fontSize:18 , bold:true},
              
              {
                //pageBreak:'after',
                text:[
                {text : 'Text 1',color:'red'},
                {text : 'Text 2',color:'blue'},
                {text : 'Text 3',bold:true}
              ]},
              {
                marginTop:15,
                color:'red',
                preserveLeadingSpaces:true,//for \t
                //alignment:'',
                //stack:[
                    text:'\tผู้สื่อข่าวรายงาน เมื่อวันที่ 20 มี.ค. 66 ว่า สำนักงานป้องกันควบคุมโรคที่ 6 ชลบุรี กรมควบคุมโรค กระทรวงสาธารณสุข ให้ข้อมูลเรื่อง ซีเซียม 137 คืออะไร ว่า ซีเซียม 137 เป็นสารกัมมันตรังสี ที่มีลักษณะโลหะอ่อนมาก สีทองเงิน เป็นของเหลวที่อุณหภูมิห้อง แต่มักจะจับตัวกับคลอไรด์กลายเป็นผงผลึก ปล่อยรังสีเบต้า และแกมม่า ใช้ในโรงงาน นอกจากนี้ ยังใช้เป็นเครื่องมือทางการแพทย์รักษามะเร็ง'
                    
                //]
              },
              {preserveLeadingSpaces:true,text:'\tสำหรับอันตรายจากการสัมผัสสารกัมมันตรังสี สำนักงานป้องกันควบคุมโรคที่ 6 ชลบุรี กรมควบคุมโรค กระทรวงสาธารณสุข ระบุว่า อันตรายจากการสัมผัสสารกัมมันตรังสี ซีเซียม 137 ได้รับอันตรายมากหรือน้อยขึ้นอยู่กับปริมาณของรังสี ชนิดของรังสีที่ได้รับ'},
              {preserveLeadingSpaces:true,text:'\tในกรณีสัมผัสปริมาณมาก ส่งผลกระทบต่อระบบเลือด กดไขกระดูก ระบบประสาท ชักเกร็ง และอาจเสียชีวิตได้'},
              {pageBreak:'before',text: 'ซีเซียม 137 คืออะไร?',fontSize:18 , bold:true , alignment:'center' ,marginTop:10},
              {
                //alignment:'justify',
                columnGap: 10,
                lineHeight: 0.8,
                characterSpacing:0.35,
                columns:[
                    {
                        //alignment:'right',
                        stack:[
                            {preserveLeadingSpaces:true,text:'\tผู้สื่อข่าวรายงาน เมื่อวันที่ 20 มี.ค. 66 ว่า สำนักงานป้องกันควบคุมโรคที่ 6 ชลบุรี กรมควบคุมโรค กระทรวงสาธารณสุข ให้ข้อมูลเรื่อง ซีเซียม 137 คืออะไร ว่า ซีเซียม 137 เป็นสารกัมมันตรังสี ที่มีลักษณะโลหะอ่อนมาก สีทองเงิน เป็นของเหลวที่อุณหภูมิห้อง แต่มักจะจับตัวกับคลอไรด์กลายเป็นผงผลึก ปล่อยรังสีเบต้า และแกมม่า ใช้ในโรงงาน นอกจากนี้ ยังใช้เป็นเครื่องมือทางการแพทย์รักษามะเร็ง'},
                            
                        ]
                    },
                    {
                        
                        //alignment:'left',
                        stack:[
                            {preserveLeadingSpaces:true, text:'\tสำหรับอันตรายจากการสัมผัสสารกัมมันตรังสี สำนักงานป้องกันควบคุมโรคที่ 6 ชลบุรี กรมควบคุมโรค กระทรวงสาธารณสุข ระบุว่า อันตรายจากการสัมผัสสารกัมมันตรังสี ซีเซียม 137 ได้รับอันตรายมากหรือน้อยขึ้นอยู่กับปริมาณของรังสี ชนิดของรังสีที่ได้รับ\n\n'},
                            {preserveLeadingSpaces:true,text:'\tในกรณีสัมผัสปริมาณมาก ส่งผลกระทบต่อระบบเลือด กดไขกระดูก ระบบประสาท ชักเกร็ง และอาจเสียชีวิตได้'}
                        ]
                    }
                ]
              }
              //{text : 'text portrait paragraph',pageOrientation:'portrait',pageBreak:'before'}//pageBreak = new page
            ],
            // styles: {
            //   header: {
            //     font: "thSarabunNew",
            //     bold: true,
            //     color: "red",
            //     fontSize: 22,
            //     alignment: "center",
            //   },
            // },
            defaultStyle: {
              font: "thSarabunNew",
              fontSize: 16,
            },
          })
          .open();
    });
  }
  