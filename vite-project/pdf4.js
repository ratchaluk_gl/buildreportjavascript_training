import { pdfMake } from "./lib/pdfmake";
import Swal, { SweetAlertArrayOptions,SweetAlertIcon } from "sweetalert2";
import moment from "moment/moment";
import 'moment/locale/th';
moment.locale('th');

export function genPDF4(element) {
    element.addEventListener('click', () => {

        const tableHeader = [
            {text:'ID',alignment:'center',fillColor:'blue',color:'white'},
            {text:'FullName',alignment:'center',fillColor:'blue',color:'white'},
            {text:'Status',alignment:'center',fillColor:'blue',color:'white'},
            {text:'Bodies',alignment:'center',fillColor:'blue',color:'white'}
        ];

        const staffs = [
            {ID:'1',FullName:'John Wick',Status:'arlive',Bodies:'229'},
            {ID:'2',FullName:'John Doe',Status:'arlive',Bodies:'13'},
            {ID:'3',FullName:'John Smith',Status:'arlive',Bodies:'45'},
            {ID:'4',FullName:'John Michel',Status:'arlive',Bodies:'66'}
        ];
        
        let tableBody = [];
        tableBody = staffs.map(({ ID,FullName,Status,Bodies })=>{
            return [
                { text: ID, alignment:"center" },
                { text: FullName, },
                { text: Status, alignment:"center" },
                { text: Bodies, alignment:"center" },
            ];
        });

        tableBody.unshift(tableHeader);//add to top

        pdfMake.createPdf({
            header:(currentPage,pageCount,pageSize)=>{
                return {
                    columns:[
                        { text:'บทความน่าสนใจ',margin:10 },
                        { text:'หน้า '+ currentPage +'/'+ pageCount, alignment:'right',margin:10 }
                    ]
                }
            },
            footer:(currentPage,pageCount,pageSize)=>{
                return {
                    columns:[
                        { text:'ที่มา : https://www.facebook.com/Hfocus.org',margin:10 },
                        { text : moment().format('lll') ,margin:10 , alignment:'center'},
                        { text:'หน้า '+ currentPage +'/'+ pageCount, alignment:'right',margin:10 }
                    ]
                }
            },
            content:[
                { 
                    text:'Daily Report',
                    fontSize:22,
                    alignment:'center',
                    decoration:'underline',
                    decorationStyle:'dotted',
                    color:'red' 
                },
                {
                    marginTop:20,
                    //layout:'lightHorizontalLines',
                    table:{
                        headerRows:1,//replete header row to any pages
                        widths:[50, '*',100,100],
                        body://[
                            // tableHeader,
                            // tableRows,
                            // [
                            //     {text:'1',alignment:'center'},
                            //     {text:'John Wick'},
                            //     {text:'arlive',alignment:'center'},
                            //     {text:'229',alignment:'center'}
                            // ],
                            // [
                            //     {text:'2',alignment:'center'},
                            //     {text:'Vincton Continental'},
                            //     {text:'arlive',alignment:'center'},
                            //     {text:'22',alignment:'center'}
                            // ],
                            // [
                            //     {text:'3',alignment:'center'},
                            //     {text:'House Keeper'},
                            //     {text:'arlive',alignment:'center'},
                            //     {text:'98',alignment:'center'}
                            // ]
                        //],
                        tableBody,
                    }
                }
            ],
            defaultStyle: {
              font: "thSarabunNew",
              fontSize: 16,
            }
        }).open();
    });
  }
  