import { pdfMake } from "./lib/pdfmake";
import { textToBase64Barcode } from "./lib/jsbarcode";
import axios from "axios";
import Swal from "sweetalert2";
import moment from "moment/moment";
import 'moment/locale/th';

moment.locale('th');

export function genPDF5(element) {
    element.addEventListener('click', async () => {

        let hospitals =[];
        //axios
        const response = await axios.get('https://api.codingthailand.com/api/hospital2?page=1&page_size=100');

        hospitals = response.data.data;

        const tableHeader = [
            {text:'ID',alignment:'center',fillColor:'yellow',color:'black'},
            {text:'Code',alignment:'center',fillColor:'yellow',color:'black'},
            {text:'Hospital Name',alignment:'center',fillColor:'yellow',color:'black'}
        ];

        let tableBody = [];
        tableBody = hospitals.map(({ id,code,h_name })=>{
            return [
                { text: id, alignment:"center" },
                { image: textToBase64Barcode(code,"CODE39") ,width:80 },
                //{ text: code, alignment:"center"},
                { text: h_name, alignment:"left" }
            ]
        });

        tableBody.unshift(tableHeader);//add to top

        pdfMake.createPdf({
            header:(currentPage,pageCount,pageSize)=>{
                return {
                    columns:[
                        { text:'Reports',margin:10 },
                        { text:'หน้า '+ currentPage +'/'+ pageCount, alignment:'right',margin:10 }
                    ]
                }
            },
            footer:(currentPage,pageCount,pageSize)=>{
                return {
                    columns:[
                        { text:'ที่มา : https://www.facebook.com/Hfocus.org',margin:10 },
                        { text : moment().format('lll') ,margin:10 , alignment:'center'},
                        { text:'หน้า '+ currentPage +'/'+ pageCount, alignment:'right',margin:10 }
                    ]
                }
            },
            content:[
                { 
                    text:'Daily Report',
                    fontSize:22,
                    alignment:'center',
                    decoration:'underline',
                    decorationStyle:'dotted',
                    color:'red' 
                },
                {
                    marginTop:20,
                    //layout:'lightHorizontalLines',
                    table:{
                        headerRows:1,//replete header row to any pages
                        widths:['auto', 'auto','*'],
                        body:tableBody,
                    }
                }
            ],
            defaultStyle: {
              font: "thSarabunNew",
              fontSize: 16,
            }
        }).open();
    });
  }
  