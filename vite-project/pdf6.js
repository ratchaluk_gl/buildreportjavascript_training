import { pdfMake } from "./lib/pdfmake";
import { textToBase64Barcode } from "./lib/jsbarcode";
import axios from "axios";
import Swal from "sweetalert2";
import moment from "moment/moment";
import 'moment/locale/th';

moment.locale('th');

export function genPDF6(element) {
    element.addEventListener('click', () => {
        pdfMake.tableLayouts = {
            headerTableLayout:{
                defaultBorder: false,
                paddingTop: (i ,node)=>{
                    return -1;
                },
                paddingBottom: (i ,node)=>{
                    return -1;
                }
            }
        }
        pdfMake.createPdf({
            header:(currentPage,pageCount)=>{
                return { text: 'หน้า '+ currentPage +'/'+ pageCount ,alignment:'center' }
            },
            footer:(currentPage,pageCount)=>{
                if(currentPage === pageCount){//last page
                    return {
                        //marginTop: -130,
                        margin:[40,-130,40,0],
                        columnGap: -15,
                        columns:[
                            [
                                { text: 'ในนาม The Continental Newyork' ,alignment:'center' ,marginTop:20},
                                {
                                    marginTop:50,
                                    columnGap:5,
                                    columns:[
                                        [
                                            { 
                                                marginTop:4,
                                                canvas: [
                                                    {
                                                        type: 'line',
                                                        x1: 0, y1: -4,
                                                        x2: 90, y2: -4,
                                                        lineWidth: 1,
                                                        lineColor:'lightgray'
                                                    },
                                                ] 
                                            },
                                            { text: 'ผู้จ่ายเงิน',alignment:'center' }
                                        ],
                                        [
                                            { 
                                                marginTop:4,
                                                canvas: [
                                                    {
                                                        type: 'line',
                                                        x1: 0, y1: -4,
                                                        x2: 90, y2: -4,
                                                        lineWidth: 1,
                                                        lineColor:'lightgray'
                                                    },
                                                ] 
                                            },
                                            { text: 'วันที่',alignment:'center' }
                                        ],
                                    ]
                                }
                            ],
                            [
                                {image:'logo',width:120,alignment:'center'}
                            ],
                            [
                                { text:'ในนามกลุ่มรับจ้างอิสระ',alignment:'center' ,marginTop:20 },
                                {
                                    marginTop:50,
                                    columnGap:5,
                                    columns:[
                                        [
                                            { 
                                                marginTop:4,
                                                canvas: [
                                                    {
                                                        type: 'line',
                                                        x1: 0, y1: -4,
                                                        x2: 90, y2: -4,
                                                        lineWidth: 1,
                                                        lineColor:'lightgray'
                                                    },
                                                ] 
                                            },
                                            { text: 'ผู้รับเงิน',alignment:'center' }
                                        ],
                                        [
                                            
                                            { text: moment().format('ll'),alignment:'center' ,marginTop:-14,},
                                            { 
                                                marginTop:4,
                                                canvas: [
                                                    {
                                                        type: 'line',
                                                        x1: 0, y1: -4,
                                                        x2: 90, y2: -4,
                                                        lineWidth: 1,
                                                        lineColor:'lightgray'
                                                    },
                                                ] 
                                            },
                                            { text: 'วันที่',alignment:'center' }
                                        ],
                                    ]
                                }
                            ]
                        ]
                    }
                }
                else{
                    return { text : 'ข้อมูล ณ วันที่ '+ moment().format('lll'), alignment:'center' }
                }
            },
            images:{
                logo:'https://codingthailand.com/site/img/logo_cct.png'
            },
            content:[
                {
                    columnGap:30,
                    columns:[
                        {
                            //lineHeight:0.9,
                            stack:[
                                { image:'logo',width:65},
                                { text:'บริษัท โทรคมนาคมแห่งชาติ จำกัด (มหาชน)' ,marginTop:5},
                                { text:'99 หมู่ที่ 2 แขวงทุ่งสองห้อง เขตหลักสี่ กทม' },
                                { text: '10120' },
                                { text: 'เลขประจำตัวผู้เสียภาษี 0000000000000' },
                                { text: 'เบอร์ติดต่อ 0999999999' },
                                { text: 'เว็ปไซต์ https://www.ntplc.co.th' }
                            ]
                        },
                        {
                            stack: [
                                { text: 'ใบเสร็จรับเงิน', fontSize:22 , color:'green',alignment:'center',marginTop:17 ,lineHeight:0.7},
                                { text: 'ต้นฉบับ', color:'green',alignment:'center',marginBottom:10},
                                { 
                                    //marginBottom: 5,
                                    canvas: [
                                        {
                                            type: 'line',
                                            x1: 0, y1: -5,
                                            x2: 230, y2: -5,
                                            lineWidth: 1,
                                            lineColor:'lightgray'
                                        },
                                        
                                    ] 
                                },
                                { 
                                    //layout:'noBorders',
                                    layout: 'headerTableLayout',
                                    table : {
                                        widths:[80, '*'],
                                        //widths:['35%', '65%'],
                                        body:[
                                            
                                            [
                                                {text:'เลขที่',alignment:'left'/*,margin:[5,5,0,0]*/ ,color:'green'},
                                                {text:'CA12132132121' ,alignment:'left'/*,margin:[5,5,0,0]*/}
                                            ],
                                            [
                                                {text:'วันที่',alignment:'left'/*,margin:[5,-6,0,0]*/ ,color:'green'},
                                                {text:moment().format('ll') ,alignment:'left'/*,margin:[5,-6,0,0]*/}
                                            ],
                                            [
                                                {text:'ผู้ขาย',alignment:'left'/*,margin:[5,-6,0,0]*/,color:'green'},
                                                {text:'The Continental' ,alignment:'left'/*,margin:[5,-6,0,0]*/}
                                            ]
                                        ]
                                    }
                                },
                                { 
                                    canvas: [
                                        {
                                            type: 'line',
                                            x1: 0, y1: 5,
                                            x2: 230, y2: 5,
                                            lineWidth: 1,
                                            lineColor:'lightgray'
                                        },
                                    ] 
                                },
                                {
                                    image: textToBase64Barcode("9874563214568","CODE39"),
                                    alignment:'center',
                                    marginTop:10,
                                    width:200,
                                    //height:30,
                                    //displayValue:false
                                },
                            ]
                        }
                    ]
                },
                [
                   
                    { text: 'ลูกค้า' ,color:'green',marginTop:5},
                    { text: 'คณะเทคโนโลยีสารสนเทศ สถาบันเทคโนโลยีพระจอมเกล้าเจ้าคุณทหารลาดกระบัง' },
                    { text: 'เลขที่ 1 ซอยฉลองกรุง 1 แขวงลาดกระบัง เขตลาดกระบัง กรุงเทพฯ 10520' }
                ],
                { 

                    canvas: [
                        {
                            type: 'line',
                            x1: 0, y1: 10,
                            x2: 520, y2: 10,
                            lineWidth: 1,
                            lineColor:'lightgray'
                        },
                    ] 
                },
                
                {
                    layout:'lightHorizontalLines',
                    //marginTop: 10,
                    
                    table:{
                        headerRows:1,
                        widths:[20, '*','auto',80,80],
                        body:[
                            [
                                { text:'#' , alignment:'center'},
                                { text:'รายละเอียด' , alignment:'center'},
                                { text:'จำนวน' , alignment:'right'},
                                { text:'ราคาต่อหน่วย' , alignment:'right'},
                                { text:'ยอดรวม' , alignment:'right'},
                            ],
                            [
                                { text:'1' , alignment:'center'},
                                { text:'รายการอาวุธที่นาย John Wick เบิกใช้ในการทำงาน 1 เดือน' , alignment:'left'},
                                { text:'359' , alignment:'right'},
                                { text:'5,000' , alignment:'right'},
                                { text:'1,795,000' , alignment:'right'},
                            ],
                            [
                                { text:'2' , alignment:'center'},
                                { text:'รายการอาวุธที่นาย John Wick ทำหาย' , alignment:'left'},
                                { text:'10' , alignment:'right'},
                                { text:'5,000' , alignment:'right'},
                                { text:'50,000' , alignment:'right'},
                            ],
                            [
                                { text:'2' , alignment:'center'},
                                { text:'รายการอาวุธที่นาย John Wick ทำหาย' , alignment:'left'},
                                { text:'10' , alignment:'right'},
                                { text:'5,000' , alignment:'right'},
                                { text:'50,000' , alignment:'right'},
                            ],
                            [
                                { text:'2' , alignment:'center'},
                                { text:'รายการอาวุธที่นาย John Wick ทำหาย' , alignment:'left'},
                                { text:'10' , alignment:'right'},
                                { text:'5,000' , alignment:'right'},
                                { text:'50,000' , alignment:'right'},
                            ],
                            [
                                { text:'2' , alignment:'center'},
                                { text:'รายการอาวุธที่นาย John Wick ทำหาย' , alignment:'left'},
                                { text:'10' , alignment:'right'},
                                { text:'5,000' , alignment:'right'},
                                { text:'50,000' , alignment:'right'},
                            ],
                            [
                                { text:'2' , alignment:'center'},
                                { text:'รายการอาวุธที่นาย John Wick ทำหาย' , alignment:'left'},
                                { text:'10' , alignment:'right'},
                                { text:'5,000' , alignment:'right'},
                                { text:'50,000' , alignment:'right'},
                            ],
                            [
                                { text:'2' , alignment:'center'},
                                { text:'รายการอาวุธที่นาย John Wick ทำหาย' , alignment:'left'},
                                { text:'10' , alignment:'right'},
                                { text:'5,000' , alignment:'right'},
                                { text:'50,000' , alignment:'right'},
                            ],
                            [
                                { text:'2' , alignment:'center'},
                                { text:'รายการอาวุธที่นาย John Wick ทำหาย' , alignment:'left'},
                                { text:'10' , alignment:'right'},
                                { text:'5,000' , alignment:'right'},
                                { text:'50,000' , alignment:'right'},
                            ],
                            [
                                { text:'2' , alignment:'center'},
                                { text:'รายการอาวุธที่นาย John Wick ทำหาย' , alignment:'left'},
                                { text:'10' , alignment:'right'},
                                { text:'5,000' , alignment:'right'},
                                { text:'50,000' , alignment:'right'},
                            ],
                            [
                                { text:'2' , alignment:'center'},
                                { text:'รายการอาวุธที่นาย John Wick ทำหาย' , alignment:'left'},
                                { text:'10' , alignment:'right'},
                                { text:'5,000' , alignment:'right'},
                                { text:'50,000' , alignment:'right'},
                            ],
                            [
                                { text:'2' , alignment:'center'},
                                { text:'รายการอาวุธที่นาย John Wick ทำหาย' , alignment:'left'},
                                { text:'10' , alignment:'right'},
                                { text:'5,000' , alignment:'right'},
                                { text:'50,000' , alignment:'right'},
                            ],
                            [
                                { text:'2' , alignment:'center'},
                                { text:'รายการอาวุธที่นาย John Wick ทำหาย' , alignment:'left'},
                                { text:'10' , alignment:'right'},
                                { text:'5,000' , alignment:'right'},
                                { text:'50,000' , alignment:'right'},
                            ],
                            [
                                { text:'2' , alignment:'center'},
                                { text:'รายการอาวุธที่นาย John Wick ทำหาย' , alignment:'left'},
                                { text:'10' , alignment:'right'},
                                { text:'5,000' , alignment:'right'},
                                { text:'50,000' , alignment:'right'},
                            ],
                            [
                                { text:'2' , alignment:'center'},
                                { text:'รายการอาวุธที่นาย John Wick ทำหาย' , alignment:'left'},
                                { text:'10' , alignment:'right'},
                                { text:'5,000' , alignment:'right'},
                                { text:'50,000' , alignment:'right'},
                            ],
                            [
                                { text:'2' , alignment:'center'},
                                { text:'รายการอาวุธที่นาย John Wick ทำหาย' , alignment:'left'},
                                { text:'10' , alignment:'right'},
                                { text:'5,000' , alignment:'right'},
                                { text:'50,000' , alignment:'right'},
                            ],
                            [
                                { text:'2' , alignment:'center'},
                                { text:'รายการอาวุธที่นาย John Wick ทำหาย' , alignment:'left'},
                                { text:'10' , alignment:'right'},
                                { text:'5,000' , alignment:'right'},
                                { text:'50,000' , alignment:'right'},
                            ],
                            [
                                { text:'2' , alignment:'center'},
                                { text:'รายการอาวุธที่นาย John Wick ทำหาย' , alignment:'left'},
                                { text:'10' , alignment:'right'},
                                { text:'5,000' , alignment:'right'},
                                { text:'50,000' , alignment:'right'},
                            ],
                            [
                                { text:'2' , alignment:'center'},
                                { text:'รายการอาวุธที่นาย John Wick ทำหาย' , alignment:'left'},
                                { text:'10' , alignment:'right'},
                                { text:'5,000' , alignment:'right'},
                                { text:'50,000' , alignment:'right'},
                            ],
                            [
                                { text:'2' , alignment:'center'},
                                { text:'รายการอาวุธที่นาย John Wick ทำหาย' , alignment:'left'},
                                { text:'10' , alignment:'right'},
                                { text:'5,000' , alignment:'right'},
                                { text:'50,000' , alignment:'right'},
                            ],
                            [
                                { text:'2' , alignment:'center'},
                                { text:'รายการอาวุธที่นาย John Wick ทำหาย' , alignment:'left'},
                                { text:'10' , alignment:'right'},
                                { text:'5,000' , alignment:'right'},
                                { text:'50,000' , alignment:'right'},
                            ],
                            [
                                { text:'2' , alignment:'center'},
                                { text:'รายการอาวุธที่นาย John Wick ทำหาย' , alignment:'left'},
                                { text:'10' , alignment:'right'},
                                { text:'5,000' , alignment:'right'},
                                { text:'50,000' , alignment:'right'},
                            ],
                            [
                                { text:'2' , alignment:'center'},
                                { text:'รายการอาวุธที่นาย John Wick ทำหาย' , alignment:'left'},
                                { text:'10' , alignment:'right'},
                                { text:'5,000' , alignment:'right'},
                                { text:'50,000' , alignment:'right'},
                            ],
                            [
                                { text:'2' , alignment:'center'},
                                { text:'รายการอาวุธที่นาย John Wick ทำหาย' , alignment:'left'},
                                { text:'10' , alignment:'right'},
                                { text:'5,000' , alignment:'right'},
                                { text:'50,000' , alignment:'right'},
                            ],
                            [
                                { text:'2' , alignment:'center'},
                                { text:'รายการอาวุธที่นาย John Wick ทำหาย' , alignment:'left'},
                                { text:'10' , alignment:'right'},
                                { text:'5,000' , alignment:'right'},
                                { text:'50,000' , alignment:'right'},
                            ],
                            [
                                { text:'2' , alignment:'center'},
                                { text:'รายการอาวุธที่นาย John Wick ทำหาย' , alignment:'left'},
                                { text:'10' , alignment:'right'},
                                { text:'5,000' , alignment:'right'},
                                { text:'50,000' , alignment:'right'},
                            ],
                            [
                                { text:'2' , alignment:'center'},
                                { text:'รายการอาวุธที่นาย John Wick ทำหาย' , alignment:'left'},
                                { text:'10' , alignment:'right'},
                                { text:'5,000' , alignment:'right'},
                                { text:'50,000' , alignment:'right'},
                            ],
                            [
                                { text:'2' , alignment:'center'},
                                { text:'รายการอาวุธที่นาย John Wick ทำหาย' , alignment:'left'},
                                { text:'10' , alignment:'right'},
                                { text:'5,000' , alignment:'right'},
                                { text:'50,000' , alignment:'right'},
                            ],
                            [
                                { text:'2' , alignment:'center'},
                                { text:'รายการอาวุธที่นาย John Wick ทำหาย' , alignment:'left'},
                                { text:'10' , alignment:'right'},
                                { text:'5,000' , alignment:'right'},
                                { text:'50,000' , alignment:'right'},
                            ],
                            [
                                { text:'2' , alignment:'center'},
                                { text:'รายการอาวุธที่นาย John Wick ทำหาย' , alignment:'left'},
                                { text:'10' , alignment:'right'},
                                { text:'5,000' , alignment:'right'},
                                { text:'50,000' , alignment:'right'},
                            ],
                            [
                                { text:'2' , alignment:'center'},
                                { text:'รายการอาวุธที่นาย John Wick ทำหาย' , alignment:'left'},
                                { text:'10' , alignment:'right'},
                                { text:'5,000' , alignment:'right'},
                                { text:'50,000' , alignment:'right'},
                            ],
                            [
                                { text:'2' , alignment:'center'},
                                { text:'รายการอาวุธที่นาย John Wick ทำหาย' , alignment:'left'},
                                { text:'10' , alignment:'right'},
                                { text:'5,000' , alignment:'right'},
                                { text:'50,000' , alignment:'right'},
                            ],
                            [
                                { text:'2' , alignment:'center'},
                                { text:'รายการอาวุธที่นาย John Wick ทำหาย' , alignment:'left'},
                                { text:'10' , alignment:'right'},
                                { text:'5,000' , alignment:'right'},
                                { text:'50,000' , alignment:'right'},
                            ],
                            [
                                { text:'2' , alignment:'center'},
                                { text:'รายการอาวุธที่นาย John Wick ทำหาย' , alignment:'left'},
                                { text:'10' , alignment:'right'},
                                { text:'5,000' , alignment:'right'},
                                { text:'50,000' , alignment:'right'},
                            ],
                            [
                                { text:'2' , alignment:'center'},
                                { text:'รายการอาวุธที่นาย John Wick ทำหาย' , alignment:'left'},
                                { text:'10' , alignment:'right'},
                                { text:'5,000' , alignment:'right'},
                                { text:'50,000' , alignment:'right'},
                            ],
                        ]
                    }
                },
                { 
                    canvas: [
                        {
                            type: 'line',
                            x1: 0, y1: 0,
                            x2: 520, y2: 0,
                            lineWidth: 1,
                            lineColor:'lightgray'
                        },
                    ] 
                },
                {
                    margin:[0,10,0,10],
                    layout:'noBorders',
                    table:{
                        widths:['*',100,100],
                        body:[
                            [
                                {  },
                                { text: 'รวมเป็นเงิน',color:'green',alignment:'right' },
                                { text: '1,845,000 บาท',alignment:'right' },
                            ],
                            [
                                { text: '(หนึ่งล้านแปดแสนสี่หมื่นห้าพันบาทถ้วน)',alignment:'left' },
                                { text: 'จำนวนเงินรวมทั้งสิ้น',color:'green',alignment:'right' },
                                { text: '1,845,000 บาท',alignment:'right' },
                            ],
                        ]
                    }
                },
                {
                    qr:'https://pdfmake.github.io/docs/0.1/document-definition-object/tables/',
                    foreground:'black',
                    background:'white',
                    fit:120,
                    version:1,
                    alignment:'center',
                    marginTop:20
                },
                { text: 'Scan Me',alignment:'center' ,fontSize:16}
                // {
                //     marginTop: 330,
                //     columnGap: -15,
                //     columns:[
                //         [
                //             { text: 'ในนาม The Continental Newyork' ,alignment:'center' },
                //             {
                //                 marginTop:70,
                //                 columnGap:5,
                //                 columns:[
                //                     [
                //                         { 
                //                             marginTop:4,
                //                             canvas: [
                //                                 {
                //                                     type: 'line',
                //                                     x1: 0, y1: -4,
                //                                     x2: 90, y2: -4,
                //                                     lineWidth: 1,
                //                                     lineColor:'lightgray'
                //                                 },
                //                             ] 
                //                         },
                //                         { text: 'ผู้จ่ายเงิน',alignment:'center' }
                //                     ],
                //                     [
                //                         { 
                //                             marginTop:4,
                //                             canvas: [
                //                                 {
                //                                     type: 'line',
                //                                     x1: 0, y1: -4,
                //                                     x2: 90, y2: -4,
                //                                     lineWidth: 1,
                //                                     lineColor:'lightgray'
                //                                 },
                //                             ] 
                //                         },
                //                         { text: 'วันที่',alignment:'center' }
                //                     ],
                //                 ]
                //             }
                //         ],
                //         [
                //             {image:'logo',width:120,alignment:'center'}
                //         ],
                //         [
                //             { text:'ในนามกลุ่มรับจ้างอิสระ',alignment:'center' },
                //             {
                //                 marginTop:70,
                //                 columnGap:5,
                //                 columns:[
                //                     [
                //                         { 
                //                             marginTop:4,
                //                             canvas: [
                //                                 {
                //                                     type: 'line',
                //                                     x1: 0, y1: -4,
                //                                     x2: 90, y2: -4,
                //                                     lineWidth: 1,
                //                                     lineColor:'lightgray'
                //                                 },
                //                             ] 
                //                         },
                //                         { text: 'ผู้รับเงิน',alignment:'center' }
                //                     ],
                //                     [
                                        
                //                         { text: moment().format('ll'),alignment:'center' ,marginTop:-14,},
                //                         { 
                //                             marginTop:4,
                //                             canvas: [
                //                                 {
                //                                     type: 'line',
                //                                     x1: 0, y1: -4,
                //                                     x2: 90, y2: -4,
                //                                     lineWidth: 1,
                //                                     lineColor:'lightgray'
                //                                 },
                //                             ] 
                //                         },
                //                         { text: 'วันที่',alignment:'center' }
                //                     ],
                //                 ]
                //             }
                //         ]
                //     ]
                // }
                
            ],
            //pageMargins:[40,20,40,230],
            defaultStyle: {
              font: "thSarabunNew",
              fontSize: 11,
              bold: true
            }
        }).open();
    });
  }
  