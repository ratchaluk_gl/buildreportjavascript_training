import { pdfMake } from "./lib/pdfmake";
import { textToBase64Barcode } from "./lib/jsbarcode";
import axios from "axios";
import Swal from "sweetalert2";
import moment from "moment/moment";
import 'moment/locale/th';

moment.locale('th');

export function genPDF7(element) {
    element.addEventListener('click', () => {
        
        pdfMake.createPdf({
            pageOrientation:'landscape',
            images:{
                logo:'https://codingthailand.com/site/img/logo_cct.png'
            },
            background:  (page)=> {
                return [
                    {
                        image: 'logo',
                        width: 300,
                        marginTop:140,
                        marginLeft:275,
                        alignment:'justify',
                        opacity:0.1,
                        //fit:[7,53]
                    }
                ];
                
            },
            content:[
                {
                    canvas:[
                        {
                            type: 'rect',
                            x: 0,
                            y: 0,
                            w: 760,
                            h: 510,
                            //r: 5,
                            //dash: { length: 5 },
                            lineColor: 'orange',
                            lineWidth:7,
                            lineCap: 'square',
                        }, 
                        
                    ]
                },
                {
                    absolutePosition:{x: 50,y:50},
                    stack:[
                        { image: 'logo',width:65 ,alignment:'center',marginTop:20},
                        { text: 'CODING CONSULTANTS (THAILAND) Co., Ltd',fontSize:35 },
                        { text: 'ขอมอบเกียรติบัตรฉบับนี้ ให้ไว้เพื่อแสดงว่า' ,marginTop:10},
                        { text: 'คุณจอนห์ วิกค์' ,fontSize:35 ,marginTop:10},
                        { text: 'ได้ผ่านการอบรมเชิงปฏิบัติการ',marginTop:10 },
                        { text: 'หลักสูตร "การประยุกต์ใช้ความรุ็ปัญญาประดิษฐ์ในการพัฒนาอะไรก็ได้ เพื่อบริการอะไรก็ได้"',marginTop:20 ,bold:false},
                        { text: 'ให้ไว้ ณ วันที่ '+ moment().format('LL'),marginTop:35 },
                        { image: 'logo',width:55,alignment:'center' },
                        { text: '(อ.เอกนรินทร์ คำคูณ)', fontSize:22},
                        { text: 'วิทยากร' ,fontSize:22}
                    ]
                }
                // [
                    
                    
                //     { image: 'logo',width:65 ,alignment:'center',marginTop:20},
                //     { text: 'CODEING CONSULTANTS (THAILAND) Co., Ltd',fontSize:35 },
                //     { text: 'ขอมอบเกียรติบัตรฉบับนี้ ให้ไว้เพื่อแสดงว่า' ,marginTop:10},
                //     { text: 'คุณจอนห์ วิกค์' ,fontSize:35 ,marginTop:10},
                //     { text: 'ได้ผ่านการอบรมเชิงปฏิบัติการ',marginTop:10 },
                //     { text: 'หลักสูตร "การประยุกต์ใช้ความรุ็ปัญญาประดิษฐ์ในการพัฒนาอะไรก็ได้ เพื่อบริการอะไรก็ได้"',marginTop:20 ,bold:false},
                //     { text: 'ให้ไว้ ณ วันที่ '+ moment().format('LL'),marginTop:35 },
                //     { image: 'logo',width:55,alignment:'center' },
                //     { text: '(อ.เอกนรินทร์ คำคูณ)', fontSize:22},
                //     { text: 'วิทยากร' ,fontSize:22}
                // ]
                
            ],
            //pageMargins:[40,20,40,230],
            defaultStyle: {
              font: "thSarabunNew",
              fontSize: 20,
              bold: true,
              alignment:'center'
        
            }
        }).open();
    });
  }
  